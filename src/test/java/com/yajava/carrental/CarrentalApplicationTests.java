package com.yajava.carrental;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import com.yajava.carrental.Controllers.UserController;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CarrentalApplicationTests {

	
	
	@LocalServerPort
	int randomServerPort;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private UserController userController;
	
	@Test
	void contextLoads() {
		assertThat(userController).isNotNull();
	}

	
	@Test
	@WithMockUser(username = "test", roles = {"USER", "ADMIN"})
	void testMessage() {
		String msg = this.restTemplate.getForObject("/user/testuser", String.class);
		assertNotEquals("teest", msg);
	}
	

	
}
