package com.yajava.carrental.Controllers;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.yajava.carrental.Model.Booking;
import com.yajava.carrental.Model.Car;
import com.yajava.carrental.Service.BookingService;

@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "http://localhost:3000/")
public class BookingController {

	private static final Logger logger = LoggerFactory.getLogger(BookingController.class);

	@Autowired
	private BookingService bookingService;

	// DONE
	@GetMapping("/findOrderById/{id}")
	public Booking getBooking(@PathVariable int id) {
		logger.info("findstartades");
		return bookingService.getBooking(id);
	}

	// DONE
	@GetMapping("/myorders")
	public List<Booking> getBooking() {
		logger.info("findAllstartades");
		return bookingService.getAllBookings();
	}

	// DONE
	@RequestMapping(path = "/ordercar", method = RequestMethod.POST, consumes = "application/json")
	public void createOrder(@RequestBody Booking booking) {
		logger.info("Created order");
		bookingService.createOrder(booking);
	}

	
	// DONE
	@PutMapping("/updateorder/{id}")
	public void updateOrder(@PathVariable int id, @RequestBody Booking booking) {
		logger.info("UpdatedOrder");
		bookingService.updateOrder(id, booking);

	}

	// DONE
	@DeleteMapping("/deleteorder")
	public void deleteOrder(@RequestParam int id) {
		logger.info("DeletedOrder");
		bookingService.deleteOrder(id);

	}

}
