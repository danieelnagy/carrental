package com.yajava.carrental.Controllers;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.yajava.carrental.Model.Car;
import com.yajava.carrental.Service.CarService;

@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "http://localhost:3000/")
public class CarController {

	private static final Logger logger = LoggerFactory.getLogger(CarController.class);

	@Autowired
	private CarService carService;

	// DONE
	@GetMapping("/findcarById/{id}")
	public Car getUser(@PathVariable int id) {
		logger.info("FindStarted");
		return carService.getCar(id);
	}

	// DONE
	@GetMapping("/cars")
	public List<Car> getCar() {
		logger.info("FindAllStarted");
		return carService.getAllCar();
	}

	// DONE
	@GetMapping("/findAllAvailableCar")
	public List<Car> getAvailableCar() {
		logger.info("FindAllAvaibleStarted");
		return carService.getAvailableCar();
	}

	// DONE
	@PutMapping("/updatecar/{id}")
	public void updateCar(@PathVariable int id, @RequestBody Car car) {
		logger.info("CarUpdated");
		carService.updateCar(id, car);
	}

	// DONE
	@RequestMapping(path = "/addcar", method = RequestMethod.POST, consumes = "application/json")
	public void createCar(@RequestBody Car car) {
		logger.info("CarCreated");
		carService.createCar(car);
	}

	// DONE
	@DeleteMapping("/deletecar/{id}")
	public void deleteCar(@PathVariable int id) {
		logger.info("CarDeleted");
		carService.deleteCar(id);
	}

}
