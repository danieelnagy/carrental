package com.yajava.carrental.Controllers;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yajava.carrental.Model.User;
import com.yajava.carrental.Service.UserService;

@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "http://localhost:3000/")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	
    @RequestMapping("/testuser")
    public String getTestUser() {
    	
        return "test";
    }

	
	@GetMapping("/findUserById/{id}")
	public User getUser(@PathVariable int id) {
		logger.info("FindStarted");
		return userService.getUser(id);
	}

	@GetMapping("/findAllUser")
	public List<User> getUser() {
		logger.info("FindAllStarted");
		return userService.getAllUser();
	}

	@GetMapping("/customers")
	public List<User> getCustomers() {
		logger.info("FIndAllCustomerStarted");
		return userService.getAllCustomer();
	}
	
	

}
