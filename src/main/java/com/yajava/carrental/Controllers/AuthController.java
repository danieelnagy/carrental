package com.yajava.carrental.Controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yajava.carrental.Model.AuthBean;

@CrossOrigin(origins={ "http://localhost:3000" })
@RestController
@RequestMapping("/api/v1/")
public class AuthController {

    @GetMapping(path = "/basicauth")
    public AuthBean helloWorldBean() {
        //throw new RuntimeException("Some Error has Happened! Contact Support at ***-***");
        return new AuthBean("You are authenticated");
    }   
}
