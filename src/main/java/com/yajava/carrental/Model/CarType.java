package com.yajava.carrental.Model;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "T_CAR_TYPE")
public class CarType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	@Column(name = "price")
	private Double price;
	@Column(name = "type_name")
	private String typeName;
	@OneToMany(mappedBy = "carType", cascade = CascadeType.ALL, orphanRemoval = true, fetch=FetchType.EAGER)
	private Set<CarType> carType;

	public CarType() {

	}

	public CarType(int id, Double price, String typeName) {
		super();
		Id = id;
		this.price = price;
		this.typeName = typeName;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@Override
	public String toString() {
		return "CarType [Id=" + Id + ", price=" + price + ", typeName=" + typeName + "]";
	}
	
	

}
