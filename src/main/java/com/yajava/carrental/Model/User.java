package com.yajava.carrental.Model;


import javax.persistence.*;
import com.sun.istack.NotNull;

@Entity
@Table(name = "T_USER")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	@Column(name = "full_name")
	private String name;
	@Column(name = "user_name")
	@NotNull
	private String userName;
	@Column(name = "password")
	@NotNull
	private String password;
	@Column(name = "mobile_number")
	private String mobileNumber;
	@Column(name = "email")
	private String email;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id")
	private UserAddress address;
	@ManyToOne
	@JoinColumn(name = "role_id", referencedColumnName = "id")
	private Roles userRoles;

	public User() {

	}

	public User(int id, String name, String userName, String password, String mobileNumber, String email,
			UserAddress address, Roles userRoles) {
		super();
		Id = id;
		this.name = name;
		this.userName = userName;
		this.password = password;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.address = address;
		this.userRoles = userRoles;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserAddress getAddress() {
		return address;
	}

	public void setAddress(UserAddress address) {
		this.address = address;
	}

	public Roles getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Roles userRoles) {
		this.userRoles = userRoles;
	}

	@Override
	public String toString() {
		return "User [Id=" + Id + ", name=" + name + ", userName=" + userName + ", password=" + password
				+ ", mobileNumber=" + mobileNumber + ", email=" + email + ", address=" + address + ", userRoles="
				+ userRoles + "]";
	}

}
