package com.yajava.carrental.Model;

import javax.persistence.*;

@Entity
@Table(name = "T_CAR")
public class Car {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	@Column(name = "car_name")
	private String name;
	@Column(name = "status")
	private boolean status;
	@Column(name = "color")
	private String color;
	@ManyToOne
	@JoinColumn(name = "type_id")
	private CarType carType;

	public Car() {
	}

	public Car(int id, String name, boolean status, String color, CarType carType) {
		super();
		Id = id;
		this.name = name;
		this.status = status;
		this.color = color;
		this.carType = carType;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public CarType getCarType() {
		return carType;
	}

	public void setCarType(CarType carType) {
		this.carType = carType;
	}

	@Override
	public String toString() {
		return "Car [Id=" + Id + ", name=" + name + ", status=" + status + ", color=" + color + ", carType=" + carType
				+ "]";
	}

}
