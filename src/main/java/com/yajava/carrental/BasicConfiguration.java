package com.yajava.carrental;



import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import com.yajava.carrental.Service.UserService;

@Configuration
@EnableWebSecurity
public class BasicConfiguration extends WebSecurityConfigurerAdapter {
	

	 @Override
	    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	        PasswordEncoder encoder =
	                PasswordEncoderFactories.createDelegatingPasswordEncoder();
	        
	        auth
	                .inMemoryAuthentication()
	                .withUser("user")
	                .password(encoder.encode("user"))
	                .roles("USER")
	                .and()
	                .withUser("admin")
	                .password(encoder.encode("admin"))
	                .roles("ADMIN");
	    }

	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http.headers().frameOptions().disable();

	        http
	                .authorizeRequests()
	                .antMatchers("/api/v1/cars").permitAll()
	                .antMatchers("/api/v1/**")
	                .hasAnyRole("USER","ADMIN")
	                .anyRequest()
	                .fullyAuthenticated()
	                .and()
	                .csrf().disable()
	                .httpBasic();
	        http.cors(c -> {
	            CorsConfigurationSource cs = request -> {
	                CorsConfiguration cc = new CorsConfiguration();
	                cc.setAllowedOrigins((Arrays.asList("*")));
	                cc.setAllowedHeaders((Arrays.asList("authorization","content-type")));
	                cc.setAllowedMethods((Arrays.asList("*")));
	                cc.addAllowedOrigin("*");
	                cc.addAllowedHeader("*");
	                cc.addAllowedMethod("*");
	                return cc;
	            };
	            c.configurationSource(cs);
	            
	        });
	    }

}