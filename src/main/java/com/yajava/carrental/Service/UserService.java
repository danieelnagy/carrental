package com.yajava.carrental.Service;

import org.springframework.stereotype.Service;
import com.yajava.carrental.Model.User;
import com.yajava.carrental.Repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User getUser(int id) {
		User user = userRepository.getById(id);
		return user;
	}

	public List<User> getAllUser() {

		List<User> userList = userRepository.findAll();
		return userList;
	}

	
	public List<User> getAllCustomer() {

		List<User> userList = getAllUser();
		for (int i = 0; i < userList.size(); i++) {

			if (userList.get(i).getUserRoles().getName().toString().equalsIgnoreCase("admin")) {
				userList.remove(i);
			}

		}
		return userList;
	}
	

	// TODO addUser/sortByRole/getAll/updateUser/removeUser

}
