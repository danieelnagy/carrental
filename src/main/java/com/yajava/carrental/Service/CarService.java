package com.yajava.carrental.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.yajava.carrental.Model.Car;
import com.yajava.carrental.Repository.CarRepository;



@Service
public class CarService {

	
	@Autowired
	private CarRepository carRepository;
	
	public void Save(Car car) {
		carRepository.save(car);
	}
	
	public Car getCar(int id) {
		Car car = carRepository.getById(id);
		return car;
	}
	
	public List<Car> getAllCar() {
		
		List<Car> carList = carRepository.findAll();
		return carList;
	}
	
	public List<Car> getAvailableCar() {
		
		
		List<Car> availableCars = getAllCar();
		for(int i = 0; i < availableCars.size();i++) {
			if(availableCars.get(i).isStatus() != true) {
				availableCars.remove(i);
			}
		}
		
		return availableCars;
	}
	
	public void updateCar(int id, Car car) {
		
		Car existingCar = carRepository.getById(id);
		existingCar.setColor(car.getColor());
		existingCar.setName(car.getName());
		Save(existingCar);
		
	}
	
	public void createCar(Car car) {
		
		Save(car);
		System.out.println("Created");
		
	}
	
	public void deleteCar(int id) {
		
		carRepository.deleteById(id);
		System.out.println("Deleted");
	}
	
}
