package com.yajava.carrental.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yajava.carrental.Model.Booking;
import com.yajava.carrental.Model.Car;
import com.yajava.carrental.Model.User;
import com.yajava.carrental.Repository.BookingRepository;

@Service
public class BookingService {

	
	@Autowired
	BookingRepository bookingRepository;
	
	
	public void Save(Booking booking) {
		
		bookingRepository.save(booking);
	}
	
	
	public void createOrder(Booking booking) {
		
		Save(booking);
		System.out.println("Done");
	}
	
	public void deleteOrder(int id) {
		bookingRepository.deleteById(id);
	}
	
	public void updateOrder(int id, Booking booking) {
		Booking existingBooking = bookingRepository.getById(id);
		
		existingBooking.setCreationDate(booking.getCreationDate());
		existingBooking.setStartDate(booking.getStartDate());
		existingBooking.setEndDate(booking.getEndDate());
		Save(booking);
		
	}
	

	public Booking getBooking(int id) {
		Booking booking = bookingRepository.getById(id);
		return booking;
	}

	public List<Booking> getAllBookings() {

		List<Booking> bookingList = bookingRepository.findAll();
		return bookingList;
	}

}
