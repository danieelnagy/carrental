package com.yajava.carrental.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.yajava.carrental.Model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	
	
}
