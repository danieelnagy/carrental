package com.yajava.carrental.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yajava.carrental.Model.Car;


@Repository
public interface CarRepository extends JpaRepository<Car, Integer> {

}
