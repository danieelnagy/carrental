package com.yajava.carrental.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.yajava.carrental.Model.Booking;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {

}
