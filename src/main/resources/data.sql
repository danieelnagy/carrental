INSERT INTO T_CAR_TYPE (price, type_name) VALUES
  (899.99, 'Suv'),
  (699.99, 'Mellan kombi'),
  (499.99, 'Små elbil');
  
  INSERT INTO T_CAR (color, car_name, status, type_id) VALUES
  ('Svart', 'Volvo', 1, 2),
  ('Vit', 'Volvo', 0, 1),
  ('Svart', 'wAudi', 1, 1),
  ('Grön', 'Skoda', 0, 3),
  ('Röd', 'Vw', 0, 2),
  ('Svart', 'Vw', 0, 3);
 
  INSERT INTO T_ROLES (name) VALUES
  ('USER'),
  ('ADMIN');
  
  INSERT INTO T_ADDRESS (street, city, state, postal_code, country) VALUES
  ('Hemvagen', 'Husum', 'Vnorrland', 89631, 'Sv'),
  ('Hemvagen', 'Husum', 'Vnorrland', 89631, 'Sv'),
  ('AndersHem', 'Hem', 'Vetej', 89630, 'Sv');
  
  
 INSERT INTO T_USER (full_name, user_name, password, mobile_number, email, address_id, role_id) VALUES
  ('user', 'Madisonuser', 'user', '46722316870', 'medi@boss.se', 1, 1),
  ('admin', 'Danieluser', 'admin', '46722316871', 'dani@boss.se', 2, 2);
  
  
  INSERT INTO T_BOOKING (creation_date, end_date, start_date, sum, car_id, user_id) VALUES
  (now(), now(), now()+interval 1 day, 899.99, 1, 1);
 

  
  
  
  
  
  
  